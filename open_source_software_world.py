#!/usr/bin/env python3

"""
Dieses wikifolio investiert hauptsächlich in Firmen, die langfristig Open-Source-Software - insbesondere Linux - entwickeln, einsetzen, vertreiben oder unterstützen.
Durch den weit gestreuten Einsatz von Linux ergibt sich auch eine entsprechende Streuung des Risikos. Vom kleinen asiatischen Halbleiterproduzenten bis zu den größten Investmentgesellschaften und IT Firmen kann alles vertreten sein.

Die Auswahl der möglichen Aktien für das Portfolio basiert vor allem auf der Aktivität der Firmen in verschiedenen Open-Source Communities und Foundations, zum Anderen aber auch auf eigenem Wissen und der Erfahrung aus über 25 Jahren im Linux-Umfeld.

Um psychologische Komponenten in der Entscheidungsfindung möglichst auszuschliessen, basiert diese auf der Ausgabe eines Skriptes, das selbstverständlich auch als Open-Source-Software veröffentlicht wird.
"""


import shelve
import os
import time
from functools import reduce
import yahooquery as yq
from datetime import datetime
import pandas as pd
import numpy as np
import math
from pprint import pprint
from collections import OrderedDict
from currency_converter import CurrencyConverter, SINGLE_DAY_ECB_URL
import statistics

base_money = 460000

linux_stocks = {
    "1+1 DRILLISCH AG O.N.": "DE0005545503",
    "A10 NETWORKS INC.": "US0021211018",
    "ABB LTD ADR/1 SF 2,50": "US0003752047",
    "ACCENTURE PLC": "IE00B4BNMY34",
    "ACTIA GROUP SA INH.EO-,75": "FR0000076655",
    "ADOBE INC.": "US00724F1012",
    "ADVANCED MIC.DEV. DL-,01": "US0079031078",
    "ADVA OPT.NETW.SE O.N.": "DE0005103006",
    "AIRBNB INC. DL-,01": "US0090661010",
    "AISIN CORP.": "JP3102000001",
    "ALIBABA GR.HLDG SP.ADR 8": "US01609W1027",
    "ALLIANZ AG VNA O.N.": "DE0008404005",
    "ALPHABET INC C": "US02079K1079",
    "ALPS ELECTRIC CO.": "JP3126400005",
    "ALTAIR ENGINEERING INC.": "US0213691035",
    "AMAZON.COM INC. DL-,01": "US0231351067",
    "AMER. EXPRESS DL -,20": "US0258161092",
    "APPLE COMPUTER INC": "US0378331005",
    "ARISTA NETWORKS DL-,0001": "US0404131064",
    "ATLASSIAN CORP. A DL -,10": "GB00BZ09BD16",
    "ATOS SE NOM. EO 1": "FR0000051732",
    "AT&T INC.": "US00206R1023",
    "AUTODESK INC.": "US0527691069",
    "AUTOM. DATA PROC. DL -,10": "US0530151036",
    "BAIDU A ADR DL-,000000625": "US0567521085",
    "BAY.MOTOREN WERKE VZO": "DE0005190037",
    "BECHTLE AG O.N.": "DE0005158703",
    "BLACKROCK CL. A DL -,01": "US09247X1019",
    "BNP PARIBAS INH. EO 2": "FR0000131104",
    "BOOKING HLDGS DL-,008": "US09857L1089",
    "BOOZ ALL.HAM. CL.A DL-001": "US0995021062",
    "BROADCOM INC. DL-,001": "US11135F1012",
    "BROADRIDGE FINL SOL.DL-01": "US11133T1034",
    "CAPITAL ONE FINL DL-,01": "US14040H1059",
    "CHANGE HEALTH. DL-,001": "US15912K1007",
    "CHARTER COMMUNICATIONS INC.": "US16119P1084",
    "CHINA MOBILE (HK) HD-,10": "HK0941009539",
    "CHINA TELECOM": "CNE1000002V2",
    "CHINA UNICOM (HONG KONG) LTD.": "HK0000049939",
    "CIENA CORP. NEW DL-,01": "US1717793095",
    "CIRRUS LOGIC INC.": "US1727551004",
    "CISCO SYSTEMS": "US17275R1023",
    "CITIGROUP INC. DL -,01": "US1729674242",
    "CITRIX SYSTEMS DL-,001": "US1773761002",
    "CLOUDERA INC.": "US18914U1007",
    "CLOUDFLARE INC. A DL-,001": "US18915M1071",
    "CME GROUP INC. DL-,01": "US12572Q1058",
    "COGNIZANT TECHNOLOGY SOLUTIONS": "US1924461023",
    "COMCAST": "US20030N1019",
    "COMMVAULT SYSTEMS INC.": "US2041661024",
    "CONTINENTAL AG O.N.": "DE0005439004",
    "CYBERARK": "IL0011334468",
    "DAIMLER": "DE0007100000",
    "DATADOG INC. A DL-,00001": "US23804L1035",
    "DELL TECHS INC. C DL-,01": "US24703L2025",
    "DELTA ELECTRONICS (THAI.)PCL REG. SHARES (NVDRS) B": "TH0528010R18",
    "DENSO CORP": "JP3551500006",
    "DEUTSCHE BANK AG NA O.N.": "DE0005140008",
    "DISNEY (WALT) CO.": "US2546871060",
    "DT.TELEKOM AG NA": "DE0005557508",
    "EBAY INC. DL-,001": "US2786421030",
    "ELASTIC N.V.": "NL0013056914",
    "ELECT.DE FRANCE": "FR0010242511",
    "ELEKTROBIT GROUP OYJ": "FI0009007264",
    "EPAM SYSTEMS INC.": "US29414B1044",
    "EQUINIX INC. DL-,001": "US29444U7000",
    "ERICSSON A (FRIA)": "SE0000108649",
    "EXTREME NETWORKS INC.": "US30226D1063",
    "F5 NETWORKS INC. O.N.": "US3156161024",
    "FACEBOOK INC.": "US30303M1027",
    "FASTLY INC. CL.ADL-,00002": "US31188V1008",
    "FEDEX CORP. DL-,10": "US31428X1063",
    "FOXCONN INTERCONNECT TECH.LTD. REGISTERED SHS CL.D": "KYG3R83K1037",
    "FREDDIE MAC": "US3134003017",
    "FUJITSU LTD YN": "JP3818000006",
    "GENL EL. CO. DL -,06": "US3696041033",
    "GODADDY INC. CL.A DL-,001": "US3802371076",
    "GOLDMAN SACHS": "US38141G1040",
    "HANOVER INSUR. GRP DL-,01": "US4108671052",
    "HARTFORD FINL SVCS GRP": "US4165151048",
    "HENSOLDT AG INH O.N.": "DE000HAG0005",
    "HEWLETT PACKARD ENTERPRISE": "US42824C1099",
    "HITACHI LTD": "JP3788600009",
    "HONDA MOTOR CO": "JP3854600008",
    "HSBC HOLDINGS": "GB0005405286",
    "HUBSPOT INC. DL-,001": "US4435731009",
    "HYUNDAI MOT.0,5N.VTG GDRS": "USY384721251",
    "IHS MARKIT LTD DL -,01": "BMG475671050",
    "INFOSYS LTD. ADR/1 IR5": "US4567881085",
    "ING GROEP NV CVA EO -,24": "NL0011821202",
    "INSPUR INTL NEW HD-,01": "KYG4820C1309",
    "INTEL CORP. DL-,001": "US4581401001",
    "INTL BUS. MACH. DL-,20": "US4592001014",
    "INTUIT INC. DL-,01": "US4612021034",
    "JD.COM. INC. A": "KYG8208B1014",
    "JP MORGAN CH. DL 1": "US46625H1005",
    "JUNIPER NETWORKS DL-,01": "US48203R1041",
    "JVC KENWOOD CORP.": "JP3386410009",
    "KDDI CORP.": "JP3496400007",
    "KERLINK EO -,27": "FR0013156007",
    "KINGDEE ITL SOFTW.G.SUBD.": "KYG525681477",
    "KINGSOFT COR.LTD DL-,0005": "KYG5264Y1089",
    "KUKA AG": "DE0006204407",
    "LEAR CORP.": "US5218652049",
    "LENOVO GROUP HD-,025": "HK0992009065",
    "LG ELECTRONICS GDR": "US50186Q2021",
    "MAHINDRA+MAHIN.GDR/REG.S": "USY541641194",
    "MAIL RU. GROUP": "US5603172082",
    "MARVELL TECH. GRP DL-,002": "US5738741041",
    "MASTERCARD": "US57636Q1040",
    "MAZDA MOTOR CORP": "JP3868400007",
    "MERCK KGAA O.N.": "DE0006599905",
    "MICRON TECHN. INC. DL-,10": "US5951121038",
    "MICROSOFT CORP. DL -,001": "US5949181045",
    "MITSUBISHI ELECTRIC CORP": "JP3902400005",
    "MITSUBISHI MOTOR": "JP3899800001",
    "MONGODB INC. CL.A": "US60937P1066",
    "MORGAN STANLEY": "US6174464486",
    "NATL INSTRUMENTS DL-,01": "US6365181022",
    "NCC GROUP PLC LS -,01": "GB00B01QGK86",
    "NEC CORP YN": "JP3733000008",
    "NETAPP INC.": "US64110D1046",
    "NETFLIX INC. DL-,001": "US64110L1061",
    "NIO INC.A S.ADR DL-,00025": "US62914V1061",
    "NIPPON TELEGRAPH & TELEPHONE": "JP3735400008",
    "NOKIA CORP. EO-,06": "FI0009000681",
    "NOMURA HOLDINGS": "JP3762600009",
    "NORDIC SEMICONDUCTOR": "NO0003055501",
    "NSK LTD.": "JP3720800006",
    "NTT DATA CORP.": "JP3165700000",
    "NUTANIX INC. A": "US67059N1081",
    "NVIDIA CORP. DL-,001": "US67066G1040",
    "NXP SEMICONDUCTORS EO-,20": "NL0009538784",
    "OKTA INC. CL.A O.N.": "US6792951054",
    "ORACLE CORP. DL-,01": "US68389X1054",
    "ORANGE INH. EO 4": "FR0000133308",
    "PALO ALTO NETWORKS": "US6974351057",
    "PANASONIC CORP": "JP3866800000",
    "PAYPAL": "US70450Y1038",
    "PEGASYSTEMS INC. REGISTERED SHARES DL -,01": "US7055731035",
    "PELOTON INTE.A DL-,000025": "US70614W1009",
    "PING AN INSURANCE": "CNE1000003X6",
    "PORSCHE AUTOM.HLDG VZO": "DE000PAH0038",
    "POSTE ITALIANE SPA EO-,51": "IT0003796171",
    "QT GROUP PLC EO-,10": "FI4000198031",
    "QUALCOMM INC. DL-,0001": "US7475251036",
    "QUALYS INC. DL -,001": "US74758T3032",
    "RAKUTEN INC.": "JP3967200001",
    "RENESAS ELEC": "JP3164720009",
    "REPLY S.P.A. EO 0,13": "IT0005282865",
    "RIBBON COMMUNIC. DL-,0001": "US7625441040",
    "RICOH CO LTD": "JP3973400009",
    "SALESFORCE.COM DL-,001": "US79466L3024",
    "SAMSUNG EL. GDR": "US7960508882",
    "SAP SE": "DE0007164600",
    "SBERBANK": "US80585Y3080",
    "SEAGATE TEC.HLD.DL-,00001": "IE00BKVD2N49",
    "SELECTIVE INS. GRP DL 2": "US8163001071",
    "SES S.A. FDR A": "LU0088087324",
    "SHOPIFY A SUB.VTG": "CA82509L1076",
    "SIEMENS AG NA": "DE0007236101",
    "SIRIUS XM HLDGS DL-,001": "US82968B1035",
    "SOFTBANK CORP.": "JP3732000009",
    "SONY GROUP CORP.": "JP3435000009",
    "S+P GLOBAL INC. DL 1": "US78409V1044",
    "SPIRENT COMMUNIC.LS-,0333": "GB0004726096",
    "SPLUNK INC. DL-,001": "US8486371045",
    "SPOTIFY TECH. S.A. EUR 1": "LU1778762911",
    "STMICROELECTRONICS": "NL0000226223",
    "SUSE S.A. DL 1": "LU2333210958",
    "SUZUKI MOTOR CORP": "JP3397200001",
    "SWISSCOM NA": "CH0008742519",
    "SYNOPSYS INC.": "US8716071076",
    "TALEND SA UNSP.ADR EO-,01": "US8742242071",
    "TARGET CORP. DL-,0833": "US87612E1064",
    "TELECOM ITALIA": "IT0003497168",
    "TELEFONICA INH. EO 1": "ES0178430E18",
    "TENCENT HLDG. LTD": "KYG875721634",
    "TEXAS INSTR. DL 1": "US8825081040",
    "THALES S.A. EO 3": "FR0000121329",
    "TINEXTA S.P.A.": "IT0005037210",
    "TOSHIBA CORP": "JP3592200004",
    "TOYOTA MOTOR CO": "JP3633400001",
    "TRAVELERS COMPANIES": "US89417E1091",
    "TUFIN SOFTWARE TE IS-,015": "IL0011571556",
    "TWITTER": "US90184L1026",
    "UBER TECH. DL-,00001": "US90353T1007",
    "UBS GROUP": "CH0244767585",
    "VERIZON COMM. INC. DL-,10": "US92343V1044",
    "VMWARE": "US9285634021",
    "VODAFONE": "GB00BH4HKS39",
    "VOLKSWAGEN AG VORZUGSAKTIEN": "DE0007664039",
    "VONAGE HLDGS CORP. DL-,01": "US92886T2015",
    "WESTN DIGITAL DL-,01": "US9581021055",
    "WIPRO LTD ADR /1 IR 2": "US97651M1099",
    "WISEKEY INTL B SF -,05": "CH0314029270",
    "XILINX INC. DL-,01": "US9839191015",
    "YANDEX N.V. CL.A DL -,01": "NL0009805522",
    "ZALANDO SE": "DE000ZAL1111",
    "ZTE CORP.": "CNE1000004Y2",
}

tickercache = os.path.join(
        os.path.dirname(
            os.path.abspath(__file__)
            ),
        '.tickers.cache'
        )
historycache = os.path.join(
        os.path.dirname(
            os.path.abspath(__file__)
            ),
        '.history.cache'
        )
now = datetime.now()
date = now.strftime("%Y%m%d")
with shelve.open(historycache) as cache:
    for i in cache.keys():
        if not i.startswith(f'{date}-'):
            del cache[i]


CC = CurrencyConverter(SINGLE_DAY_ECB_URL, decimal=False)


# enhanced version of VIDYA from
# https://github.com/freqtrade/technical/blob/master/technical/indicators/indicators.py
def VIDYA(dataframe, length=9, select=True, cmo_length=None):
    """
    Source: https://www.tradingview.com/script/64ynXU2e/
    Author: Tushar Chande
    Pinescript Author: KivancOzbilgic
    Variable Index Dynamic Average VIDYA
    To achieve the goals, the indicator filters out the market
    fluctuations (noises) by averaging the price values of the periods,
    over which it is calculated.
    In the process, some extra value (weight) is added to the average prices,
    as it is done during calculations of all weighted indicators, such as EMA,
    LWMA, and SMMA.
    But during the VIDIYA indicator's calculation, every period's price
    receives a weight increment adapted to the current market's volatility .
    select: True = CMO, False= StDev as volatility index
    usage:
      dataframe['VIDYA'] = VIDYA(dataframe)
    """

    if cmo_length is None:
        cmo_length = length

    df = dataframe.copy()
    alpha = 2 / (length + 1)
    df["momm"] = df["close"].diff()
    df["m1"] = np.where(df["momm"] >= 0, df["momm"], 0.0)
    df["m2"] = np.where(df["momm"] >= 0, 0.0, -df["momm"])

    df["sm1"] = df["m1"].rolling(cmo_length).sum()
    df["sm2"] = df["m2"].rolling(cmo_length).sum()

    df["chandeMO"] = 100 * (df["sm1"] - df["sm2"]) / (df["sm1"] + df["sm2"])
    if select:
        df["k"] = abs(df["chandeMO"]) / 100
    else:
        df["k"] = df["close"].rolling(length).std()
    df.fillna(0.0, inplace=True)

    df["VIDYA"] = 0.0
    for i in range(length, len(df)):

        df["VIDYA"].iat[i] = (
            alpha * df["k"].iat[i] * df["close"].iat[i]
            + (1 - alpha * df["k"].iat[i]) * df["VIDYA"].iat[i - 1]
        )

    return df["VIDYA"]


def _get_online_ticker_symbol(isin: str) -> str:
    for i in range(0, 10):
        time.sleep(0.5*i)
        try:
            data = yq.search(isin, first_quote=True)
        except Exception:
            pass
        else:
            return data
    raise Exception(f'Failed to download ticker for isin {isin}.')


def get_ticker_symbol(isin: str) -> str:
    with shelve.open(tickercache) as cache:
        if isin not in cache:
            data = _get_online_ticker_symbol(isin)
            cache[isin] = data['symbol']
        return cache[isin]


def _get_online_ticket_data(ticker: str) -> tuple:
    tickerdata = yq.Ticker(ticker, asynchronous=True)
    df = tickerdata.history(period='2y', interval='1d')
    summary_detail = tickerdata.summary_detail
    return (df, summary_detail)


def get_ticket_data(ticker: str) -> tuple:
    index = f'{date}-{ticker}'
    with shelve.open(historycache) as cache:
        if index not in cache:
            cache[index] = _get_online_ticket_data(ticker)
        return cache[index]


def analyze_dataframe(df: pd.DataFrame) -> bool:

    df['buy'] = False
    df['vidya_9'] = VIDYA(df, length=9, cmo_length=9)
    df['vidya_50'] = VIDYA(df, length=50, cmo_length=9)

    conditions = []
    conditions.append(
            (df['vidya_9'] > df['vidya_50'])
            )
    conditions.append(
            (df['vidya_50'] > (df['vidya_50'].shift(int(365/2)) * 1.15))
            )
    conditions.append(
            (df['vidya_50'] > df['vidya_50'].shift(30))
            )
    conditions.append(
            (df['volume'] > 0)
            )

    for i in range(0, 14):
        conditions.append(
                (df['vidya_50'].shift(i) >= df['vidya_50'].shift(i + 1))
            )

    df.loc[:, 'buy'] = reduce(lambda x, y: x & y, conditions)

    return df.iloc[-1]['buy']


buys = {}
for title, isin in linux_stocks.items():
    ticker = get_ticker_symbol(isin)
    df, summary_detail = get_ticket_data(ticker)

    summary_detail = summary_detail[ticker]
    buy = analyze_dataframe(df)
    if (buy):
        buys[title] = summary_detail


marketcaps = {}
marketprices = {}

for title, summary_detail in buys.items():

    marketcap = summary_detail['marketCap']
    currency = summary_detail['currency']
    marketprice = summary_detail['regularMarketPreviousClose']
    if currency != 'EUR':
        marketcap = CC.convert(marketcap, currency.upper(), 'EUR')
        marketprice = CC.convert(marketprice, currency.upper(), 'EUR')
        currency = 'EUR'

    marketcaps[title] = marketcap
    marketprices[title] = marketprice

hm = (max(marketcaps.values()) + statistics.harmonic_mean(marketcaps.values()))/2
limited_marketcaps = {x: y if y < hm else hm for x, y in marketcaps.items()}

marketcap_sum = sum(limited_marketcaps.values())
marketcaps_percent = {x: y/marketcap_sum for x, y in limited_marketcaps.items()}
marketcaps_money = {x: base_money * y for x, y in marketcaps_percent.items()}
marketcaps_stocks_count = {
        x: math.floor(y / marketprices[x])
        if math.floor(y / marketprices[x]) > 0 else 1
        for x, y in marketcaps_money.items()
                           }

sorted_marketcaps_stocks_count = OrderedDict(
        sorted(
            marketcaps_stocks_count.items(),
            key=lambda t: marketcaps_percent[t[0]]
            )
        )
print(sum([marketprices[x] * y for x, y in marketcaps_stocks_count.items()]))

for k, v in marketcaps_stocks_count.items():
    isin = linux_stocks[k]
    mp = marketprices[k]
    print(f'({isin}) {k}: {v} * {mp}')


# {'VG': {'maxAge': 1, 'priceHint': 2, 'previousClose': 14.7,
#         'open': 14.55, 'dayLow': 14.32, 'dayHigh': 14.69,
#         'regularMarketPreviousClose': 14.7, 'regularMarketOpen': 14.55,
#         'regularMarketDayLow': 14.32, 'regularMarketDayHigh': 14.69, 'payoutRatio': 0.0,
#         'beta': 0.720279, 'forwardPE': 57.36, 'volume': 1377459, 'regularMarketVolume': 1377459,
#         'averageVolume': 2107457, 'averageVolume10days': 2348083, 'averageDailyVolume10Day': 2348083,
#         'bid': 14.4, 'ask': 15.25, 'bidSize': 3200, 'askSize': 1800,
#         'marketCap': 3611600896,
#         'fiftyTwoWeekLow': 9.52, 'fiftyTwoWeekHigh': 15.72, 'priceToSalesTrailing12Months': 2.882096,
#         'fiftyDayAverage': 14.409706, 'twoHundredDayAverage': 13.542753,
#         'currency': 'USD',
#         'fromCurrency': None, 'toCurrency': None, 'lastMarket': None,
#         'algorithm': None, 'tradeable': False}}
